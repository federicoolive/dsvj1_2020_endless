#include "loop.h"
#include <iostream>
#include <time.h>
#include "configuraciones/configuraciones.h"
#include "menu/menues.h"
#include "juego/juego.h"
#include "lenguaje/texto.h"

namespace endless
{
	namespace loop
	{
		void juego()
		{
			txt::actualizarLenguaje();
			config::init();
			menues::menuActual[menues::menuInmersion] = menues::MENU::PRINCIPAL;
			while (!WindowShouldClose() && config::enJuego)
			{
				if (IsKeyPressed(KEY_M))
				{
					config::musicaOn = !config::musicaOn;
				}
				if (IsKeyPressed(KEY_N))
				{
					config::efectosOn = !config::efectosOn;
				}
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				{
					std::cout << GetMousePosition().y << std::endl;
				}

				config::tiempo[0] = clock();
				switch (menues::menuActual[menues::menuInmersion])
				{
				case menues::MENU::PRINCIPAL:

					menues::principal::update();

					break;
				case menues::MENU::ENDLESS:

					juego::update();

					break;
				case menues::MENU::OPCIONES:

					menues::opciones::update();

					break;
				case menues::MENU::CREDITOS:

					menues::creditos::update();

					break;
				default:
					break;
				}
			}
			config::deInit();
			CloseWindow();

		}
	}
}
