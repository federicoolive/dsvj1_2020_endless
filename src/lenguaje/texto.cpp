#include "texto.h"
#include "configuraciones/configuraciones.h"

namespace endless
{
	namespace txt
	{
		char titulo[] = "Endless";
		LENGUAJE lenguaje = LENGUAJE::ESPA�OL;

		int tamFuente = 30;
		char chBtnJugar[6];
		char chBtnOpciones[9];
		char chBtnCreditos[9];
		char chBtnSalir[6];
		char empezarPartida[28];

		std::string muteMusica;
		std::string muteEfectos;
		std::string chresoluciones;

		void actualizarLenguaje()
		{
			tamFuente = 30 * config::escalado;
			switch (lenguaje)
			{
			case LENGUAJE::INGLES:
				chBtnJugar[0] = 'P';
				chBtnJugar[1] = 'l';
				chBtnJugar[2] = 'a';
				chBtnJugar[3] = 'y';
				chBtnJugar[4] = '\0';
				chBtnJugar[5] = '\0';

				chBtnOpciones[0] = 'O';
				chBtnOpciones[1] = 'p';
				chBtnOpciones[2] = 't';
				chBtnOpciones[3] = 'i';
				chBtnOpciones[4] = 'o';
				chBtnOpciones[5] = 'n';
				chBtnOpciones[6] = 's';
				chBtnOpciones[7] = '\0';
				chBtnOpciones[8] = '\0';

				chBtnCreditos[0] = 'C';
				chBtnCreditos[1] = 'r';
				chBtnCreditos[2] = 'e';
				chBtnCreditos[3] = 'd';
				chBtnCreditos[4] = 'i';
				chBtnCreditos[5] = 't';
				chBtnCreditos[6] = 's';
				chBtnCreditos[7] = '\0';
				chBtnCreditos[8] = '\0';

				chBtnSalir[0] = 'E';
				chBtnSalir[1] = 'x';
				chBtnSalir[2] = 'i';
				chBtnSalir[3] = 't';
				chBtnSalir[4] = '\0';
				chBtnSalir[5] = '\0';

				empezarPartida[0] = 'P';
				empezarPartida[1] = 'r';
				empezarPartida[2] = 'e';
				empezarPartida[3] = 's';
				empezarPartida[4] = 's';
				empezarPartida[5] = ' ';
				empezarPartida[6] = 'e';
				empezarPartida[7] = 'n';
				empezarPartida[8] = 't';
				empezarPartida[9] = 'e';
				empezarPartida[10] = 'r';
				empezarPartida[11] = ' ';
				empezarPartida[12] = 't';
				empezarPartida[13] = 'o';
				empezarPartida[14] = ' ';
				empezarPartida[15] = 'S';
				empezarPartida[16] = 't';
				empezarPartida[17] = 'a';
				empezarPartida[18] = 'r';
				empezarPartida[19] = 't';
				empezarPartida[20] = '\0';
				empezarPartida[21] = '\0';
				empezarPartida[22] = '\0';
				empezarPartida[23] = '\0';
				empezarPartida[24] = '\0';
				empezarPartida[25] = '\0';
				empezarPartida[26] = '\0';
				empezarPartida[27] = '\0';

				muteMusica = "Press M to mute music.";
				muteEfectos = "Press N to mute effects.";
				chresoluciones = "Resolutions:";
				break;

			case LENGUAJE::ESPA�OL:
				chBtnJugar[0] = 'J';
				chBtnJugar[1] = 'u';
				chBtnJugar[2] = 'g';
				chBtnJugar[3] = 'a';
				chBtnJugar[4] = 'r';
				chBtnJugar[5] = '\0';

				chBtnOpciones[0] = 'O';
				chBtnOpciones[1] = 'p';
				chBtnOpciones[2] = 'c';
				chBtnOpciones[3] = 'i';
				chBtnOpciones[4] = 'o';
				chBtnOpciones[5] = 'n';
				chBtnOpciones[6] = 'e';
				chBtnOpciones[7] = 's';
				chBtnOpciones[8] = '\0';

				chBtnCreditos[0] = 'C';
				chBtnCreditos[1] = 'r';
				chBtnCreditos[2] = 'e';
				chBtnCreditos[3] = 'd';
				chBtnCreditos[4] = 'i';
				chBtnCreditos[5] = 't';
				chBtnCreditos[6] = 'o';
				chBtnCreditos[7] = 's';
				chBtnCreditos[8] = '\0';

				chBtnSalir[0] = 'S';
				chBtnSalir[1] = 'a';
				chBtnSalir[2] = 'l';
				chBtnSalir[3] = 'i';
				chBtnSalir[4] = 'r';
				chBtnSalir[5] = '\0';

				empezarPartida[0] = 'P';
				empezarPartida[1] = 'r';
				empezarPartida[2] = 'e';
				empezarPartida[3] = 's';
				empezarPartida[4] = 'i';
				empezarPartida[5] = 'o';
				empezarPartida[6] = 'n';
				empezarPartida[7] = 'a';
				empezarPartida[8] = ' ';
				empezarPartida[9] = 'e';
				empezarPartida[10] = 'n';
				empezarPartida[11] = 't';
				empezarPartida[12] = 'e';
				empezarPartida[13] = 'r';
				empezarPartida[14] = ' ';
				empezarPartida[15] = 'p';
				empezarPartida[16] = 'a';
				empezarPartida[17] = 'r';
				empezarPartida[18] = 'a';
				empezarPartida[19] = ' ';
				empezarPartida[20] = 'e';
				empezarPartida[21] = 'm';
				empezarPartida[22] = 'p';
				empezarPartida[23] = 'e';
				empezarPartida[24] = 'z';
				empezarPartida[25] = 'a';
				empezarPartida[26] = 'r';
				empezarPartida[27] = '\0';

				muteMusica = "Presione M para mutear la musica.";
				muteEfectos = "Presione N para mutear los efectos.";
				chresoluciones = "Resoluciones:";

				break;
			default:
				break;
			}
		}




	}
}