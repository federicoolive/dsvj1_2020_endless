#ifndef TEXTO_H
#define TEXTO_H
#include <iostream>

namespace endless
{
	namespace txt
	{
		enum class LENGUAJE { INGLES, ESPA�OL, LIMITE };
		extern LENGUAJE lenguaje;

		extern int tamFuente;

		extern char titulo[];
		extern char chBtnJugar[];
		extern char chBtnOpciones[];
		extern char chBtnCreditos[];
		extern char chBtnSalir[];
		extern char empezarPartida[28];

		extern std::string muteMusica;
		extern std::string muteEfectos;

		extern std::string chresoluciones;

		void actualizarLenguaje();
	}
}

#endif