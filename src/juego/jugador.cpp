#include "jugador.h"
#include <time.h>
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "escenario/escenario.h"
#include "juego.h"
#include "menu/menues.h"
#include "juego/objetos.h"

namespace endless
{
	namespace jugador
	{
		JUGADOR jugador01;

		void init()
		{
			jugador::jugador01.texture.height = 632 / 5 * config::escalado;
			jugador::jugador01.texture.width = jugador::jugador01.texture.height * 3.3978f;	// Relación ancho alto png

			jugador::jugador01.frameRec.height = jugador::jugador01.texture.height;
			jugador::jugador01.frameRec.width = jugador::jugador01.texture.width / 3;
			jugador::jugador01.frameRec.x = 0;
			jugador::jugador01.frameRec.y = 0;

			jugador01.activo = true;
			jugador01.frameActual = 1;
			jugador01.rec = jugador01.frameRec;

			escenario::posActual = 1;
			jugador01.rec.x = GetScreenWidth() * 1 / 10;
			jugador01.rec.y = escenario::posJugador[escenario::posActual];
			jugador01.enMovimiento = false;

			jugador01.puntaje = 0;
		}

		void update()
		{
			// ACTUALIZACIÓN DE IMAGEN (FRAME)
			if (jugador01.proximo && config::tiempo[0] > config::tiempo[1] + jugador01.tiempoPorFrame)
			{
				jugador01.frameActual++;
				config::tiempo[1] = clock();
				jugador::jugador01.frameRec.x = jugador::jugador01.frameRec.width * jugador::jugador01.frameActual;
				if (jugador01.frameActual >= jugador01.maxFrames)
				{
					jugador01.proximo = !jugador01.proximo;
				}
			}
			else if (!jugador01.proximo && config::tiempo[0] > config::tiempo[1] + jugador01.tiempoPorFrame)
			{
				jugador01.frameActual--;
				config::tiempo[1] = clock();
				jugador::jugador01.frameRec.x = jugador::jugador01.frameRec.width * jugador::jugador01.frameActual;
				if (jugador01.frameActual <= 0)
				{
					jugador01.proximo = !jugador01.proximo;
				}
			}
			
			// ----------------------------------------------------------------------------

			for (int i = 0; i < escenario::maxSuelos; i++)
			{
				if (jugador01.rec.y == escenario::posJugador[i])
				{
					if (config::tiempo[0] > config::tiempo[2] + escenario::tiempoPuntaje)
					{
						config::tiempo[2] = clock();
						jugador01.puntaje += escenario::selvaSuelo[i].multiplicadorPuntos;
					}
				}
			}

			// ----------------------------------------------------------------------------

			input();

		}
		void input()
		{
			//if (jugador01.enMovimiento == 0)
			{
				if (IsKeyPressed(KEY_UP))
				{
					if (escenario::posActual < escenario::maxSuelos - 1)
					{
						escenario::posActual++;
						jugador01.enMovimiento = 2;
						if (config::efectosOn)
						{
							PlaySound(config::efectoSaltar);
						}
					}
				}
				if (IsKeyPressed(KEY_DOWN))
				{
					if (escenario::posActual > 0)
					{
						escenario::posActual--;
						jugador01.enMovimiento = 1;
						if (config::efectosOn)
						{
							PlaySound(config::efectoSaltar);
						}
					}
				}
				if (IsKeyDown(KEY_LEFT))
				{
					if (jugador01.rec.x > 50)
					{
						jugador01.rec.x -= 10;
					}
				}
				if (IsKeyDown(KEY_RIGHT))
				{
					if (jugador01.rec.x < GetScreenWidth() / 2)
					{
						jugador01.rec.x += 10;
					}
				}
				if (IsKeyPressed(KEY_SPACE))
				{
					if (juego::enPausa)
					{
						menues::pausa::tiempoPausa[1] = clock();
						obj::tiempo += (menues::pausa::tiempoPausa[0] - menues::pausa::tiempoPausa[0]);
					}
					else
					{
						menues::pausa::tiempoPausa[0] = clock();
					}
					juego::enPausa = !juego::enPausa;
				}
			}
			//else
			{
				moverJugador();
			}
		}

		void moverJugador()
		{
			if (jugador01.enMovimiento == 2)
			{
				jugador01.rec.y -= movimientoVertical;
				if (jugador01.rec.y < escenario::posJugador[escenario::posActual])
				{
					jugador01.rec.y = escenario::posJugador[escenario::posActual];
					jugador01.enMovimiento = 0;
				}
			}
			else if (jugador01.enMovimiento == 1)
			{
				jugador01.rec.y += movimientoVertical;
				if (jugador01.rec.y > escenario::posJugador[escenario::posActual])
				{
					jugador01.rec.y = escenario::posJugador[escenario::posActual];
					jugador01.enMovimiento = 0;
				}
			}
		}

		void draw()
		{
			DrawTextureRec(jugador::jugador01.texture, jugador::jugador01.frameRec, { jugador01.rec.x,jugador01.rec.y }, WHITE);
			//DrawCircleLines(jugador01.rec.x + jugador01.rec.width / 2, jugador01.rec.y + jugador01.rec.height / 2, jugador01.rec.width / 4, GREEN);
		}
	}
}