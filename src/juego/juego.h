#ifndef JUEGO_H
#define JUEGO_H
#include "raylib.h"

namespace endless
{
	namespace juego
	{
		extern bool enPausa;
		extern bool gameOver;
		void init();
		void update();
		void input();
		void draw();

	}
}

#endif