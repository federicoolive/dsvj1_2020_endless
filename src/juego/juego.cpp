#include "juego.h"
#include <time.h>
#include "jugador.h"
#include "objetos.h"
#include "escenario/escenario.h"
#include "menu/menues.h"
#include "lenguaje/texto.h"
#include "configuraciones/configuraciones.h"

namespace endless
{
	namespace juego
	{
		bool enJuego = false;
		bool enPausa = false;
		bool gameOver = false;

		void init()
		{
			PlayMusicStream(config::musicaJuego);
			enJuego = false;
			enPausa = false;
			gameOver = false;

			obj::init();
			jugador::init();
			escenario::init();
		}
		void update()
		{
			input();

			if (config::musicaOn)
				UpdateMusicStream(config::musicaJuego);

			if (!enJuego)
			{
				
			}
			else
			{
				if (gameOver)
				{
					menues::gameover::update();
				}
				else
				{
					if (enPausa)
					{
						menues::pausa::update();
					}
					else
					{
						jugador::update();
						obj::update();
						escenario::update();
					}
				}
			}

			draw();
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
			{
				enJuego = true;
			}
		}

		void draw()
		{
			BeginDrawing();
			ClearBackground(WHITE);

			escenario::draw();
			obj::draw();
			jugador::draw();

			if (!enJuego)
			{
				DrawText(txt::empezarPartida, GetScreenWidth() / 2 - MeasureText(txt::empezarPartida, txt::tamFuente * 2) / 2, GetScreenHeight() / 2 - 50, txt::tamFuente * 2, SKYBLUE);
			}

			if (enPausa)
			{
				menues::pausa::draw();
			}
			if (gameOver)
			{
				menues::gameover::draw();
			}
			EndDrawing();
		}
	
	}
}