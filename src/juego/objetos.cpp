#include "objetos.h"
#include <iostream>
#include <time.h>
#include "configuraciones/configuraciones.h"
#include "escenario/escenario.h"

namespace endless
{
	namespace obj
	{
		ITEM fruta[maxItems];
		ITEM enemigo[maxItems];
		ITEM obstaculo[maxItems];
		
		int tiempo;
		int tiempoanim;
		int tiempoEspera;
		int posActual = -1;
		int posAnterior = 0;
		int tipoActual = -1;
		int tipoAnterior = 0;
		void init()
		{
			tiempoanim = clock();
			tiempo = clock();
			for (int i = 0; i < maxItems; i++)
			{
				fruta[i].activo = false;
				fruta[i].textura = fruta[0].textura;
				fruta[i].puntaje = 5;
				fruta[i].maxFrames = 5;
				fruta[i].frameRec.width = fruta[i].textura.width / fruta[i].maxFrames;
				fruta[i].frameRec.height = fruta[i].textura.height;

				enemigo[i].activo = false;
				enemigo[i].maxFrames = 12;
				enemigo[i].textura = enemigo[0].textura;
				enemigo[i].puntaje = -100;
				enemigo[i].frameRec.width = enemigo[i].textura.width / enemigo[i].maxFrames;
				enemigo[i].frameRec.height = enemigo[i].textura.height;

				obstaculo[i].activo = false;
				obstaculo[i].textura = obstaculo[0].textura;
				obstaculo[i].puntaje = 0;
				obstaculo[i].maxFrames = 1;
				obstaculo[i].frameRec.width = obstaculo[i].textura.width;
				obstaculo[i].frameRec.height = obstaculo[i].textura.height;

				tiempoEspera = 1000;
			}
		}

		void update()
		{
			for (int i = 0; i < maxItems; i++)
			{
				if (fruta[i].activo)	// Actualizo Pos y frame de Frutas activas
				{
					fruta[i].pos.x -= fruta[i].velocidad;

					if (config::tiempo[0] > tiempoanim + tiempoEspera / 10)
					{
						tiempoanim = clock();
						if (fruta[i].proximo)
						{
							fruta[i].frameActual++;
							if (fruta[i].frameActual >= fruta[i].maxFrames)
							{
								fruta[i].frameActual -= 2;
								fruta[i].proximo = false;
							}
						}
						else
						{
							fruta[i].frameActual--;
							if (fruta[i].frameActual <= 0)
							{
								fruta[i].proximo = true;
							}
						}
					}
				}

				if (enemigo[i].activo)	// Actualizo Pos y frame de Enemigos activos
				{
					enemigo[i].pos.x -= enemigo[i].velocidad;

					if (config::tiempo[0] > tiempoanim + tiempoEspera / 10)
					{
						tiempoanim = clock();
						if (enemigo[i].proximo)
						{
							enemigo[i].frameActual++;
							if (enemigo[i].frameActual >= enemigo[i].maxFrames)
							{
								enemigo[i].frameActual--;
								enemigo[i].proximo = false;
							}
						}
						else
						{
							enemigo[i].frameActual--;
							if (enemigo[i].frameActual <= 0)
							{
								enemigo[i].proximo = true;
								enemigo[i].frameActual++;
							}
						}
					}
				}

				if (obstaculo[i].activo)	// Actualizo Pos y frame de Obstaculo activos
				{
					obstaculo[i].pos.x -= obstaculo[i].velocidad;

					if (config::tiempo[0] > tiempoanim + tiempoEspera / 10)
					{
						tiempoanim = clock();
						if (obstaculo[i].proximo)
						{
							obstaculo[i].frameActual++;
							if (obstaculo[i].frameActual >= obstaculo[i].maxFrames)
							{
								obstaculo[i].frameActual -= 2;
								obstaculo[i].proximo = false;
							}
						}
						else
						{
							obstaculo[i].frameActual--;
							if (obstaculo[i].frameActual <= 0)
							{
								obstaculo[i].proximo = true;
							}
						}
					}
				}
			}

			if (config::tiempo[0] > tiempo + tiempoEspera)
			{
				tiempo = clock();

				posAnterior = posActual;
				tipoAnterior = tipoActual;
				while (posAnterior == posActual)	// No repite posicion
				{
					posActual = GetRandomValue(0, 2);
				}
				while (tipoActual == tipoAnterior)	// No repite mismo objeto
				{
					tipoActual = GetRandomValue(0, 2);
				}
				switch (tipoActual)
				{
				case 0:

					for (int i = 0; i < maxItems; i++)
					{
						if (!fruta[i].activo)
						{
							fruta[i].activo = true;
							fruta[i].velocidad = escenario::selvaSuelo[posActual].velocidad;
							fruta[i].pos = escenario::posObjetos[posActual];
							break;
						}
					}

					break;
				case 1:

					for (int i = 0; i < maxItems; i++)
					{
						if (!enemigo[i].activo)
						{
							enemigo[i].activo = true;
							enemigo[i].velocidad = escenario::selvaSuelo[posActual].velocidad;
							enemigo[i].pos = escenario::posObjetos[posActual];
							break;
						}
					}
					break;
				case 2:

					for (int i = 0; i < maxItems; i++)
					{
						if (!obstaculo[i].activo)
						{
							obstaculo[i].activo = true;
							obstaculo[i].velocidad = escenario::selvaSuelo[posActual].velocidad;
							obstaculo[i].pos = escenario::posObjetos[posActual];
							break;
						}
					}
					break;
				default:
					break;
				}
			}
		}

		void draw()
		{
			for (int i = 0; i < maxItems; i++)
			{
				if (fruta[i].activo)
				{
					DrawTextureRec(fruta[i].textura, { fruta[i].frameRec.width * fruta[i].frameActual, fruta[i].frameRec.y, fruta[i].frameRec.width, fruta[i].frameRec.height }, { fruta[i].pos.x - fruta[i].frameRec.width / 2, fruta[i].pos.y - fruta[i].frameRec.height / 2 }, WHITE);
				}

				if (enemigo[i].activo)
				{
					DrawTextureRec(enemigo[i].textura, { enemigo[i].frameRec.width * enemigo[i].frameActual, enemigo[i].frameRec.y, enemigo[i].frameRec.width, enemigo[i].frameRec.height }, { enemigo[i].pos.x - enemigo[i].frameRec.width / 2, enemigo[i].pos.y - enemigo[i].frameRec.height / 2 }, WHITE);
					//DrawRectangle(enemigo[i].pos.x, enemigo[i].pos.y, enemigo[i].frameRec.width, enemigo[i].frameRec.height, GREEN);
				}

				if (obstaculo[i].activo)
				{
					DrawTextureRec(obstaculo[i].textura, { obstaculo[i].frameRec.width * obstaculo[i].frameActual, obstaculo[i].frameRec.y, obstaculo[i].frameRec.width, obstaculo[i].frameRec.height }, { obstaculo[i].pos.x - obstaculo[i].frameRec.width / 2, obstaculo[i].pos.y - obstaculo[i].frameRec.height / 2 }, WHITE);
					//DrawRectangle(obstaculo[i].pos.x, obstaculo[i].pos.y, obstaculo[i].frameRec.width, obstaculo[i].frameRec.height, BLACK);
				}
			}
		}
	}
}