#ifndef JUGADOR_H
#define JUGADOR_H

#include "raylib.h"

namespace endless
{
	namespace jugador
	{
		const int movimientoVertical = 10;

		struct JUGADOR
		{
			bool activo = true;

			Texture2D texture;
			int frameActual = 0;
			int tiempoPorFrame = 100;
			bool proximo = false;
			int maxFrames = 2;

			int enMovimiento;	//0=sin movimiento, 1=Arriba, 2=abajo
			int textEscalado = 1;
			Rectangle frameRec;
			Rectangle rec;
			int puntaje;
		};
		extern JUGADOR jugador01;

		void init();
		void update();
		void input();
		void moverJugador();
		void draw();
	}
}


#endif