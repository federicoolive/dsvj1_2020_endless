#ifndef OBJETOS_H
#define OBJETOS_H

#include "raylib.h"

namespace endless
{
	namespace obj
	{
		const int maxItems = 10;
		extern int tiempo;
		extern int tiempoEspera;
		struct ITEM
		{
			bool activo = false;
			Vector2 pos;

			Texture2D textura;
			int frameActual = 0;
			int tiempoPorFrame = 100;
			bool proximo = true;
			int maxFrames = 5;

			Rectangle frameRec;
			int puntaje;
			int velocidad;
		};
		extern ITEM fruta[maxItems];
		extern ITEM enemigo[maxItems];
		extern ITEM obstaculo[maxItems];
		
		void init();
		void update();
		void draw();
	}
}


#endif