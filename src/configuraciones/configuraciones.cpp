#include "configuraciones/configuraciones.h"
#include "lenguaje/texto.h"
#include "juego/jugador.h"
#include "escenario/escenario.h"
#include "juego/objetos.h"
#include "menu/menues.h"

namespace endless
{
	namespace config
	{
		Music musicaMenu;
		Music musicaJuego;
		Sound efectoSaltar;
		Sound efectoGameOver;
		Sound efectoPickUp;
		Sound efectoPickUpFail;

		bool musicaOn;
		bool efectosOn;

		int tiempo[3] = { 0 };

		Vector2 pantallaInicial[5];
		int resolActual;

		bool enJuego;
		int fpsTarget = 60;
		float escalado;

		bool FullScreen = false;
		Vector2 posCentro;
		
		void init()
		{
			pantallaInicial[0] = {  800,  450 };
			pantallaInicial[1] = { 1280,  720 };
			pantallaInicial[2] = { 1366,  768 };
			pantallaInicial[3] = { 1600,  900 };
			pantallaInicial[4] = { 1920, 1080 };
			enJuego = true;
			resolActual = 1;

			InitWindow(pantallaInicial[resolActual].x, pantallaInicial[resolActual].y, txt::titulo);

			posCentro.x = GetWindowPosition().x + pantallaInicial[resolActual].x / 2;
			posCentro.y = GetWindowPosition().y + pantallaInicial[resolActual].y / 2;

			SetTargetFPS(fpsTarget);
			InitAudioDevice();
			menues::menuInmersion = 0;
			jugador::jugador01.texture = LoadTexture("res/assets/jugadores/jugador_tucan.png");
			
			escenario::selvaFondo[0].textura = LoadTexture("res/assets/fondo/selvaFondo03.png");
			escenario::selvaFondo[1].textura = LoadTexture("res/assets/fondo/selvaFondo02.png");
			escenario::selvaFondo[2].textura = LoadTexture("res/assets/fondo/selvaFondo01.png");

			escenario::selvaSuelo[0].textura = LoadTexture("res/assets/fondo/selvaSuelo01.png");
			escenario::selvaSuelo[1].textura = LoadTexture("res/assets/fondo/selvaSuelo02.png");
			escenario::selvaSuelo[2].textura = LoadTexture("res/assets/fondo/selvaSuelo03.png");

			obj::fruta[0].textura		= LoadTexture("res/assets/items/choclo.png");
			obj::enemigo[0].textura		= LoadTexture("res/assets/items/enemigo.png");
			obj::obstaculo[0].textura	= LoadTexture("res/assets/items/obstaculo.png");

			menues::texturaBoton = LoadTexture("res/assets/botones/botonMenu.png");

			musicaMenu	= LoadMusicStream("res/assets/audio/musicaMenu.mp3");
			musicaJuego = LoadMusicStream("res/assets/audio/musicaJuego.mp3");
			musicaOn	= true;
			efectosOn	= true;
			PlayMusicStream(musicaMenu);

			efectoSaltar	  = LoadSound("res/assets/audio/efectoSaltar.wav");
			efectoGameOver	  = LoadSound("res/assets/audio/efectoGameOver.wav");
			efectoPickUp	  = LoadSound("res/assets/audio/efectoPickUp.wav");
			efectoPickUpFail  = LoadSound("res/assets/audio/efectoPickUpFail.wav");

			menues::principal::banderaEn = LoadTexture("res/assets/botones/banderaEn.png");
			menues::principal::banderaEs = LoadTexture("res/assets/botones/banderaEs.png");

			update();
		}

		void update()
		{
			// RESOLUCIÓN DE PANTALLA.
			if ((FullScreen && !IsWindowFullscreen()) || (!FullScreen && IsWindowFullscreen()))
			{
				ToggleFullscreen();
			}

			SetWindowSize(pantallaInicial[resolActual].x, pantallaInicial[resolActual].y);
			SetWindowPosition(posCentro.x - pantallaInicial[resolActual].x / 2, posCentro.y - pantallaInicial[resolActual].y / 2);

			escalado = pantallaInicial[resolActual].x / pantallaInicial[1].x;

			menues::principal::banderaEs.width = 200 * escalado;
			menues::principal::banderaEs.height = menues::principal::banderaEs.width * 0.5f;

			menues::principal::banderaEn.width = 200 * escalado;
			menues::principal::banderaEn.height = menues::principal::banderaEn.width * 0.5f;

			// escalado de texturas
			//------ Items -------

			obj::fruta[0].textura.width = 300 * config::escalado;
			obj::fruta[0].textura.height = obj::fruta[0].textura.width * 0.177777777777f;

			obj::enemigo[0].textura.width = 1000 * config::escalado;
			obj::enemigo[0].textura.height = obj::enemigo[0].textura.width * 0.059882935614588f;

			obj::obstaculo[0].textura.width = 80 * config::escalado;
			obj::obstaculo[0].textura.height = obj::obstaculo[0].textura.width * 1.4054054054f;

			//------ Fondo -------

			for (int i = 0; i < escenario::maxFondos; i++)
			{
				escenario::selvaFondo[i].textura.width = pantallaInicial[resolActual].x;
				escenario::selvaFondo[i].textura.height = pantallaInicial[resolActual].y;
			}
			for (int i = 0; i < escenario::maxSuelos; i++)
			{
				escenario::selvaSuelo[i].textura.width = pantallaInicial[resolActual].x;
				escenario::selvaSuelo[i].textura.height = pantallaInicial[resolActual].y;
			}

			//------ Inits ------
			escenario::init();
			jugador::init();
			obj::init();
			menues::init();
			txt::actualizarLenguaje();
		}

		void deInit()
		{
			UnloadTexture(jugador::jugador01.texture);
			UnloadTexture(escenario::selvaFondo[0].textura);
			UnloadTexture(escenario::selvaFondo[1].textura);
			UnloadTexture(escenario::selvaFondo[2].textura);
			UnloadTexture(escenario::selvaSuelo[0].textura);
			UnloadTexture(escenario::selvaSuelo[1].textura);
			UnloadTexture(escenario::selvaSuelo[2].textura);

			UnloadTexture(obj::fruta[0].textura);
			UnloadTexture(obj::enemigo[0].textura);
			UnloadTexture(obj::obstaculo[0].textura);

			UnloadTexture(menues::texturaBoton);
			UnloadMusicStream(musicaMenu);
			UnloadMusicStream(musicaJuego);

			UnloadSound(efectoSaltar);
			UnloadSound(efectoGameOver);
			UnloadSound(efectoPickUp);
			UnloadSound(efectoPickUpFail);

			UnloadTexture(menues::principal::banderaEn);
			UnloadTexture(menues::principal::banderaEs);
		}
	}
}