#ifndef CONFIGURACIONES_H
#define CONFIGURACIONES_H

#include "raylib.h"

namespace endless
{
	namespace config
	{
		extern Music musicaMenu;
		extern Music musicaJuego;
		extern Sound efectoSaltar;
		extern Sound efectoGameOver;
		extern Sound efectoPickUp;
		extern Sound efectoPickUpFail;

		extern bool musicaOn;
		extern bool efectosOn;

		extern int tiempo[3];

		extern Vector2 pantallaInicial[5];
		extern int resolActual;
		extern int fpsTarget;
		
		extern bool enJuego;
		extern float escalado;
		extern bool FullScreen;

		void init();
		void update();
		void deInit();
	}
}

#endif