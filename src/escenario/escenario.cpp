#include "escenario.h"
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "juego/jugador.h"
#include "juego/objetos.h"
#include "juego/juego.h"
#include "lenguaje/texto.h"

using namespace std;

namespace endless
{
	namespace escenario
	{
		FONDO selvaFondo[maxFondos];
		FONDO selvaSuelo[maxSuelos];

		Vector2 posObjetos[maxSuelos] = { {0,0}, {1,1}, {2,2} };

		int posJugador[maxSuelos];
		int posActual = 0;
		int tiempoPuntaje = 250;

		void init()
		{
			posObjetos[0] = { static_cast<float>(GetScreenWidth()), static_cast<float>(GetScreenHeight() * (500.0f / 720)) };
			posObjetos[1] = { static_cast<float>(GetScreenWidth()), static_cast<float>(GetScreenHeight() * (300.0f / 720)) };
			posObjetos[2] = { static_cast<float>(GetScreenWidth()), static_cast<float>(GetScreenHeight() * (100.0f / 720)) };

			posJugador[0] = GetScreenHeight() * 0.64f;
			posJugador[1] = GetScreenHeight() * 0.3511111111f;
			posJugador[2] = GetScreenHeight() * 0.0622222222f;

			for (int i = 0; i < maxFondos; i++)
			{
				selvaFondo[i].velocidad = i * 5 + 1;
			}

			for (int i = 0; i < maxSuelos; i++)
			{
				selvaSuelo[i].velocidad = i * 1.3f + 10;
			}

			selvaSuelo[0].multiplicadorPuntos = 1;
			selvaSuelo[1].multiplicadorPuntos = 5;
			selvaSuelo[2].multiplicadorPuntos = 10;
		}

		void update()
		{
			for (int i = 0; i < maxFondos; i++)
			{
				selvaFondo[i].pos.x += selvaFondo[i].velocidad;

				if (selvaFondo[i].pos.x + selvaFondo[2].textura.width < 0)
				{
					selvaFondo[i].pos.x = selvaFondo[2].pos.x + selvaFondo[i].textura.width;
				}
			}

			for (int i = 0; i < maxSuelos; i++)
			{
				selvaSuelo[i].pos.x += selvaSuelo[i].velocidad;

				if (selvaSuelo[i].pos.x + selvaSuelo[i].textura.width < 0)
				{
					selvaSuelo[i].pos.x = selvaSuelo[i].pos.x + selvaSuelo[i].textura.width;
				}
			}
			colisiones();
		}

		void colisiones()
		{
			// Objeto Vs Limite Izq
			for (int i = 0; i < obj::maxItems; i++)
			{
				if (obj::fruta[i].pos.x + obj::fruta[i].frameRec.width < 0)
				{
					obj::fruta[i].activo = false;
				}
				if (obj::enemigo[i].pos.x + obj::enemigo[i].frameRec.width < 0)
				{
					obj::enemigo[i].activo = false;
				}
				if (obj::obstaculo[i].pos.x + obj::obstaculo[i].frameRec.width < 0)
				{
					obj::obstaculo[i].activo = false;
				}
			}

			for (int i = 0; i < obj::maxItems; i++)
			{
				if (obj::fruta[i].activo)
				{
					if (CheckCollisionCircles({ jugador::jugador01.rec.x + jugador::jugador01.rec.width / 2,jugador::jugador01.rec.y + jugador::jugador01.rec.height / 2 }, jugador::jugador01.rec.width / 2, { obj::fruta[i].pos.x + obj::fruta[i].frameRec.width / 2,obj::fruta[i].pos.y + obj::fruta[i].frameRec.height / 2 }, obj::fruta[i].frameRec.width / 2))
					{
						obj::fruta[i].activo = false;
						jugador::jugador01.puntaje += obj::fruta[i].puntaje;
						if (config::efectosOn)
						{
							PlaySound(config::efectoPickUp);
						}
					}
				}

				if (obj::enemigo[i].activo)
				{
					if (CheckCollisionCircles({ jugador::jugador01.rec.x + jugador::jugador01.rec.width / 2,jugador::jugador01.rec.y + jugador::jugador01.rec.height / 2 }, jugador::jugador01.rec.width / 2, { obj::enemigo[i].pos.x + obj::enemigo[i].frameRec.width / 2,obj::enemigo[i].pos.y + obj::enemigo[i].frameRec.height / 2 }, obj::enemigo[i].frameRec.width / 2))
					{
						obj::enemigo[i].activo = false;
						if (!IsKeyDown(KEY_RIGHT))
						{
							jugador::jugador01.puntaje += obj::enemigo[i].puntaje;
							if (config::efectosOn)
							{
								PlaySound(config::efectoPickUpFail);
							}
						}
						else
						{
							if (config::efectosOn)
							{
								PlaySound(config::efectoPickUp);
							}
						}
					}
				}

				if (obj::obstaculo[i].activo)
				{
					if (CheckCollisionCircles({ jugador::jugador01.rec.x + jugador::jugador01.rec.width / 2,jugador::jugador01.rec.y + jugador::jugador01.rec.height / 2 }, jugador::jugador01.rec.width / 2, { obj::obstaculo[i].pos.x, obj::obstaculo[i].pos.y }, obj::obstaculo[i].frameRec.width / 2))
					{
						jugador::jugador01.puntaje += obj::obstaculo[i].puntaje;
						juego::gameOver = true;
						
						if (config::efectosOn)
						{
							PlaySound(config::efectoGameOver);
						}
					}
				}
			}
		}

		void draw()
		{
			for (int i = 0; i < maxFondos; i++)
			{
				DrawTextureRec(selvaFondo[i].textura, { selvaFondo[i].pos.x, selvaFondo[i].pos.y, static_cast<float>(selvaFondo[i].textura.width), static_cast<float>(selvaFondo[i].textura.height) }, { 0,0 }, WHITE);
			}

			for (int i = 0; i < maxSuelos; i++)
			{
				DrawTextureRec(selvaSuelo[i].textura, { selvaSuelo[i].pos.x, selvaSuelo[i].pos.y, static_cast<float>(selvaSuelo[i].textura.width), static_cast<float>(selvaSuelo[i].textura.height) }, { 0,0 }, WHITE);
			}
			
			for (int i = 0; i < maxSuelos; i++)
			{
				DrawText(TextFormat("+%i", selvaSuelo[i].multiplicadorPuntos), GetScreenWidth() * 9.2 / 10, posJugador[i], txt::tamFuente * 1.5f, WHITE);
			}
			DrawText(TextFormat("Score: %i", jugador::jugador01.puntaje), GetScreenWidth() * 3 / 4, GetScreenHeight() - 50, txt::tamFuente, BLACK);

			DrawText(&txt::muteMusica[0], 10, GetScreenHeight() - txt::tamFuente * 2, txt::tamFuente, WHITE);
			DrawText(&txt::muteEfectos[0], 10, GetScreenHeight() - txt::tamFuente, txt::tamFuente, WHITE);
		}
	}
}