#ifndef ESCENARIO_H
#define ESCENARIO_H

#include "raylib.h"

namespace endless
{
	namespace escenario
	{
		const int maxFondos = 3;
		const int maxSuelos = 3;

		struct FONDO
		{
			Vector2 pos;
			bool delante;
			Texture2D textura;
			int velocidad;
			int multiplicadorPuntos;
		};
		extern FONDO selvaFondo[maxFondos];
		extern FONDO selvaSuelo[maxSuelos];

		extern Vector2 posObjetos[maxSuelos];

		extern int posJugador[maxSuelos];
		extern int posActual;
		extern int tiempoPuntaje;

		void init();
		void update();
		void colisiones();
		void draw();
	}
}

#endif