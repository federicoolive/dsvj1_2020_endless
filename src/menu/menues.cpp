#include "menues.h"
#include <iostream>
#include <time.h>
#include "lenguaje/texto.h"
#include "configuraciones/configuraciones.h"
#include "escenario/escenario.h"
#include "juego/juego.h"
#include "juego/objetos.h"

namespace endless
{
	namespace menues
	{
		MENU menuActual[static_cast<int>(MENU::LIMITE)];
		int menuInmersion = 0;
		Vector2 mouse = { 0 };
		Texture2D texturaBoton;

		struct BOTON
		{
			Rectangle rec;
			Texture2D textura;
			Color color;
		};

		void init()
		{
			principal::init();
			opciones::init();
			creditos::init();
			pausa::init();
			gameover::init();
		}

		namespace principal
		{
			BOTON btnJugar;
			BOTON btnOpciones;
			BOTON btnCreditos;
			BOTON salir;
			BOTON idioma;

			Texture2D banderaEn;
			Texture2D banderaEs;

			void init()
			{
				texturaBoton.width = 200 * config::escalado;
				texturaBoton.height = texturaBoton.width / 2;

				btnJugar.textura = texturaBoton;
				btnOpciones.textura = texturaBoton;
				btnCreditos.textura = texturaBoton;
				salir.textura = texturaBoton;

				btnJugar.rec.width = texturaBoton.width;
				btnOpciones.rec.width = texturaBoton.width;
				btnCreditos.rec.width = texturaBoton.width;
				salir.rec.width = texturaBoton.width;

				btnJugar.rec.height = texturaBoton.height;
				btnOpciones.rec.height = texturaBoton.height;
				btnCreditos.rec.height = texturaBoton.height;
				salir.rec.height = texturaBoton.height;

				btnJugar.rec.x		= GetScreenWidth() / 2 - btnJugar.rec.width / 2;
				btnOpciones.rec.x	= GetScreenWidth() / 2 - btnOpciones.rec.width / 2;
				btnCreditos.rec.x	= GetScreenWidth() / 2 - btnCreditos.rec.width / 2;
				salir.rec.x			= GetScreenWidth() / 2 - salir.rec.width / 2;

				btnJugar.rec.y		= GetScreenHeight() * 2 / 10 - btnJugar.rec.height / 2;
				btnOpciones.rec.y	= GetScreenHeight() * 4 / 10 - btnOpciones.rec.height / 2;
				btnCreditos.rec.y	= GetScreenHeight() * 6 / 10 - btnCreditos.rec.height / 2;
				salir.rec.y			= GetScreenHeight() * 8 / 10 - salir.rec.height / 2;

				idioma.textura = banderaEs;
				idioma.rec.width = idioma.textura.width;
				idioma.rec.height = idioma.textura.height;
				idioma.rec.x = GetScreenWidth() - idioma.textura.width;
				idioma.rec.y = 0;
				idioma.color = WHITE;
			}
			void update()
			{
				if(config::musicaOn)
					UpdateMusicStream(config::musicaMenu);
				mouse = GetMousePosition();
				input();
				draw();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnJugar.rec))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENU::ENDLESS;

						juego::init();
					}
					if (CheckCollisionPointRec(mouse, idioma.rec))
					{
						if (txt::lenguaje == txt::LENGUAJE::ESPA�OL)
						{
							txt::lenguaje = txt::LENGUAJE::INGLES;
							idioma.textura = banderaEn;
						}
						else
						{
							txt::lenguaje = txt::LENGUAJE::ESPA�OL;
							idioma.textura = banderaEs;
						}
						txt::actualizarLenguaje();
					}
					if (CheckCollisionPointRec(mouse, btnOpciones.rec))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENU::OPCIONES;
					}
					if (CheckCollisionPointRec(mouse, btnCreditos.rec))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENU::CREDITOS;
					}
					if (CheckCollisionPointRec(mouse, salir.rec))
					{
						config::enJuego = false;
					}
				}
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				for (int i = 0; i < escenario::maxFondos; i++)
				{
					DrawTextureRec(escenario::selvaFondo[i].textura, { escenario::selvaFondo[i].pos.x, escenario::selvaFondo[i].pos.y, static_cast<float>(escenario::selvaFondo[i].textura.width), static_cast<float>(escenario::selvaFondo[i].textura.height) }, { 0,0 }, WHITE);
				}

				DrawTextureEx(btnJugar.textura, { btnJugar.rec.x, btnJugar.rec.y }, 0, 1, BLUE);
				DrawTextureEx(btnOpciones.textura, { btnOpciones.rec.x, btnOpciones.rec.y }, 0, 1, SKYBLUE);
				DrawTextureEx(btnCreditos.textura, { btnCreditos.rec.x, btnCreditos.rec.y }, 0, 1, ORANGE);
				DrawTextureEx(salir.textura, { salir.rec.x, salir.rec.y }, 0, 1, RED);
								
				DrawTextureEx(idioma.textura, { idioma.rec.x, idioma.rec.y }, 0, 1, idioma.color);

				DrawText(txt::chBtnJugar, btnJugar.rec.x + btnJugar.rec.width / 2 - MeasureText(txt::chBtnJugar, txt::tamFuente) / 2, btnJugar.rec.y + btnJugar.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText(txt::chBtnOpciones, btnOpciones.rec.x + btnOpciones.rec.width / 2 - MeasureText(txt::chBtnOpciones, txt::tamFuente) / 2, btnOpciones.rec.y + btnOpciones.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText(txt::chBtnCreditos, btnCreditos.rec.x + btnCreditos.rec.width / 2 - MeasureText(txt::chBtnCreditos, txt::tamFuente) / 2, btnCreditos.rec.y + btnCreditos.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText(txt::chBtnSalir, salir.rec.x + salir.rec.width / 2 - MeasureText(txt::chBtnSalir, txt::tamFuente) / 2, salir.rec.y + salir.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);

				EndDrawing();
			}
		}
		namespace opciones
		{
			BOTON btnResol0450;	//{ 800,  450 }
			BOTON btnResol0720;	//{ 1280,  720 }
			BOTON btnResol0768;	//{ 1366,  768 }
			BOTON btnResol0900;	//{ 1600,  900 }
			BOTON btnResol1080;	//{ 1920, 1080 }
			BOTON btnFullScreen;
			BOTON btnAlMenu;

			void init()
			{
				btnResol0450.textura = texturaBoton;
				btnResol0450.rec.width = btnResol0450.textura.width;
				btnResol0450.rec.height = btnResol0450.textura.height;
				btnResol0450.rec.x = GetScreenWidth() / 2 - btnResol0450.rec.width / 2;
				
				btnAlMenu = btnResol0450;
				btnResol0720 = btnResol0450;
				btnResol0768 = btnResol0450;
				btnResol0900 = btnResol0450;
				btnResol1080 = btnResol0450;
				btnFullScreen = btnResol0450;

				btnResol0450.rec.y = GetScreenHeight() * 1 / 10;
				btnResol0720.rec.y = btnResol0450.rec.y + btnResol0450.rec.height * 1;
				btnResol0768.rec.y = btnResol0450.rec.y + btnResol0450.rec.height * 2;
				btnResol0900.rec.y = btnResol0450.rec.y + btnResol0450.rec.height * 3;
				btnResol1080.rec.y = btnResol0450.rec.y + btnResol0450.rec.height * 4;
				btnFullScreen.rec.y = btnResol0450.rec.y + btnResol0450.rec.height * 5;

				btnAlMenu.rec.y = btnFullScreen.rec.y;
				btnAlMenu.rec.x = GetScreenWidth() - btnAlMenu.rec.width * 1.5f;

				btnAlMenu.color = RED;

				cambiarColorGris();
			}

			void update()
			{
				if (config::musicaOn)
					UpdateMusicStream(config::musicaMenu);

				input();
				draw();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					mouse = GetMousePosition();
					if (CheckCollisionPointRec(mouse, btnResol0450.rec))
					{
						config::resolActual = 0;
						cambiarColorGris();
						btnResol0450.color = BLUE;
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnResol0720.rec))
					{
						config::resolActual = 1;
						cambiarColorGris();
						btnResol0720.color = BLUE;
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnResol0768.rec))
					{
						config::resolActual = 2;
						cambiarColorGris();
						btnResol0768.color = BLUE;
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnResol0900.rec))
					{
						config::resolActual = 3;
						cambiarColorGris();
						btnResol0900.color = BLUE;
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnResol1080.rec))
					{
						config::resolActual = 4;
						cambiarColorGris();
						btnResol1080.color = BLUE;
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnFullScreen.rec))
					{
						config::FullScreen = !config::FullScreen;
						cambiarColorGris();
						config::update();
					}
					else if (CheckCollisionPointRec(mouse, btnAlMenu.rec))
					{
						menuInmersion--;
					}
				}
			}

			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				for (int i = 0; i < escenario::maxFondos; i++)
				{
					DrawTextureRec(escenario::selvaFondo[i].textura, { escenario::selvaFondo[i].pos.x, escenario::selvaFondo[i].pos.y, static_cast<float>(escenario::selvaFondo[i].textura.width), static_cast<float>(escenario::selvaFondo[i].textura.height) }, { 0,0 }, WHITE);
				}
				DrawText(&txt::chresoluciones[0], btnResol0450.rec.x + btnResol0450.rec.width / 2 - MeasureText(&txt::chresoluciones[0], txt::tamFuente) / 2, GetScreenHeight() * 1 / 20 - txt::tamFuente / 2, txt::tamFuente, WHITE);

				DrawTextureEx(btnResol0450.textura, { btnResol0450.rec.x, btnResol0450.rec.y }, 0, 1, btnResol0450.color);
				DrawTextureEx(btnResol0720.textura, { btnResol0720.rec.x, btnResol0720.rec.y }, 0, 1, btnResol0720.color);
				DrawTextureEx(btnResol0768.textura, { btnResol0768.rec.x, btnResol0768.rec.y }, 0, 1, btnResol0768.color);
				DrawTextureEx(btnResol0900.textura, { btnResol0900.rec.x, btnResol0900.rec.y }, 0, 1, btnResol0900.color);
				DrawTextureEx(btnResol1080.textura, { btnResol1080.rec.x, btnResol1080.rec.y }, 0, 1, btnResol1080.color);
				
				DrawText("800 x 450", btnResol0450.rec.x + btnResol0450.rec.width / 2 - MeasureText("800 x 450", txt::tamFuente) / 2, btnResol0450.rec.y + btnResol0450.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("1280 x 720", btnResol0720.rec.x + btnResol0720.rec.width / 2 - MeasureText("1280 x 720", txt::tamFuente) / 2, btnResol0720.rec.y + btnResol0720.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("1366 x 768", btnResol0768.rec.x + btnResol0768.rec.width / 2 - MeasureText("1366 x 768", txt::tamFuente) / 2, btnResol0768.rec.y + btnResol0768.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("1600 x 900", btnResol0900.rec.x + btnResol0900.rec.width / 2 - MeasureText("1600 x 900", txt::tamFuente) / 2, btnResol0900.rec.y + btnResol0900.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("1920 x 1080", btnResol1080.rec.x + btnResol1080.rec.width / 2 - MeasureText("1920 x 1080", txt::tamFuente) / 2, btnResol1080.rec.y + btnResol1080.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);

				DrawTextureEx(btnFullScreen.textura, { btnFullScreen.rec.x, btnFullScreen.rec.y }, 0, 1, btnFullScreen.color);
				DrawText("Full Screen", btnFullScreen.rec.x + btnFullScreen.rec.width / 2 - MeasureText("Full Screen", txt::tamFuente) / 2, btnFullScreen.rec.y + btnFullScreen.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);

				DrawTextureEx(btnAlMenu.textura, { btnAlMenu.rec.x, btnAlMenu.rec.y }, 0, 1, btnAlMenu.color);
				DrawText("Menu", btnAlMenu.rec.x + btnAlMenu.rec.width / 2 - MeasureText("Menu", txt::tamFuente) / 2, btnAlMenu.rec.y + btnAlMenu.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				
				DrawText(&txt::muteMusica[0], 10, GetScreenHeight() - txt::tamFuente * 2, txt::tamFuente, WHITE);
				DrawText(&txt::muteEfectos[0], 10, GetScreenHeight() - txt::tamFuente, txt::tamFuente, WHITE);

				EndDrawing();
			}
			void cambiarColorGris()
			{
				btnResol0450.color = GRAY;
				btnResol0720.color = GRAY;
				btnResol0768.color = GRAY;
				btnResol0900.color = GRAY;
				btnResol1080.color = GRAY;

				btnFullScreen.color = GRAY;
				if (config::FullScreen)
					btnFullScreen.color = BLUE;
				else
					btnFullScreen.color = GRAY;

				switch (config::resolActual)
				{
				case 0:
					btnResol0450.color = BLUE;
					break;
				case 1:
					btnResol0720.color = BLUE;

					break;
				case 2:
					btnResol0768.color = BLUE;

					break;
				case 3:
					btnResol0900.color = BLUE;

					break;
				case 4:
					btnResol1080.color = BLUE;
					break;
				default:
					break;
				}
			}
		}

		namespace creditos
		{
			void init()
			{

			}
			void update()
			{
				if (config::musicaOn)
					UpdateMusicStream(config::musicaMenu);

				if (IsKeyPressed(KEY_ONE) || IsKeyPressed(KEY_KP_1))
				{
					OpenURL("https://trello.com/b/DcZoYJlR/dsvj12020endless");
				}
				else if (IsKeyPressed(KEY_TWO) || IsKeyPressed(KEY_KP_2))
				{
					OpenURL("https://gitlab.com/federicoolive/dsvj1_2020_endless");
				}
				else if (IsKeyPressed(KEY_THREE) || IsKeyPressed(KEY_KP_3))
				{
					OpenURL("https://www.youtube.com/watch?v=aLetdk09ewc");
					OpenURL("https://www.youtube.com/watch?v=ouQ6-8VKU0A");

				}
				else if (IsKeyPressed(GetKeyPressed()) || IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					menuInmersion--;
				}
				draw();
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);
				for (int i = 0; i < escenario::maxFondos; i++)
				{
					DrawTextureRec(escenario::selvaFondo[i].textura, { escenario::selvaFondo[i].pos.x, escenario::selvaFondo[i].pos.y, static_cast<float>(escenario::selvaFondo[i].textura.width), static_cast<float>(escenario::selvaFondo[i].textura.height) }, { 0,0 }, WHITE);
				}
				DrawText("Programer: Federico Olive. - Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Programer: Federico Olive.", txt::tamFuente), 100, txt::tamFuente, RED);
				DrawText("Artist: Santiago Laffosse. - Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Artist: Santiago Laffosse.", txt::tamFuente), 150, txt::tamFuente, RED);
				DrawText("Enviroment Artist: Joel Montillo. - Ex-Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Enviroment Artist: Joel Montillo.", txt::tamFuente), 200, txt::tamFuente, RED);

				DrawText("Presione 1 para abrir Trello.", GetScreenWidth() / 2 - MeasureText("Precione 1 para abrir Trello.", txt::tamFuente), 300, txt::tamFuente, BLUE);
				DrawText("Presione 2 para abrir GitLab.", GetScreenWidth() / 2 - MeasureText("Precione 2 para abrir GitLab", txt::tamFuente), 350, txt::tamFuente, BLUE);
				DrawText("Presione 3 para abrir referencias de asset.", GetScreenWidth() / 2 - MeasureText("Presione 3 para abrir refere", txt::tamFuente), 400, txt::tamFuente, BLUE);

				DrawText("Version 1.0", 0, GetScreenHeight() - 20, txt::tamFuente/2, RED);
				EndDrawing();
			}
		}

		namespace pausa
		{
			int tiempoPausa[2];
			BOTON btnReanudar;
			BOTON btnAlMenu;

			void init()
			{
				texturaBoton.width = 200 * config::escalado;
				texturaBoton.height = texturaBoton.width / 2;

				btnReanudar.color = BLUE;
				btnAlMenu.color = PINK;

				btnReanudar.textura = texturaBoton;
				btnAlMenu.textura = texturaBoton;

				btnReanudar.rec.width = texturaBoton.width;
				btnAlMenu.rec.width = texturaBoton.width;

				btnReanudar.rec.height = texturaBoton.height;
				btnAlMenu.rec.height = texturaBoton.height;

				btnReanudar.rec.x = GetScreenWidth() / 2 - btnReanudar.rec.width * 1.5f;
				btnAlMenu.rec.x = GetScreenWidth() / 2 + btnAlMenu.rec.width * 1.5f - btnAlMenu.rec.width;

				btnReanudar.rec.y = GetScreenHeight() / 2 - btnReanudar.rec.height / 2;
				btnAlMenu.rec.y = GetScreenHeight() / 2 - btnAlMenu.rec.height / 2;
			}
			void update()
			{
				mouse = GetMousePosition();
				input();
			}

			void input()
			{
				if (IsKeyPressed(KEY_SPACE))
				{
					if (juego::enPausa)
					{
						menues::pausa::tiempoPausa[1] = clock();
						obj::tiempo += (menues::pausa::tiempoPausa[0] - menues::pausa::tiempoPausa[0]);
					}
					else
					{
						menues::pausa::tiempoPausa[0] = clock();
					}
					juego::enPausa = !juego::enPausa;
				}
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnReanudar.rec))
					{
						juego::enPausa = false;
					}
					if (CheckCollisionPointRec(mouse, btnAlMenu.rec))
					{
						menuInmersion--;
						menuActual[menuInmersion] = MENU::PRINCIPAL;
					}
				}
			}

			void draw()
			{
				DrawTextureEx(btnReanudar.textura, { btnReanudar.rec.x, btnReanudar.rec.y }, 0, 1, btnReanudar.color);
				DrawTextureEx(btnAlMenu.textura, { btnAlMenu.rec.x, btnAlMenu.rec.y }, 0, 1, btnAlMenu.color);

				DrawText("Continue", btnReanudar.rec.x + btnReanudar.rec.width / 2 - MeasureText("Continue", txt::tamFuente) / 2, btnReanudar.rec.y + btnReanudar.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("Menu", btnAlMenu.rec.x + btnAlMenu.rec.width / 2 - MeasureText("Menu", txt::tamFuente) / 2, btnAlMenu.rec.y + btnAlMenu.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
			}
		}

		namespace gameover
		{
			BOTON btnReiniciar;
			BOTON btnAlMenu;

			void init()
			{
				texturaBoton.width = 200 * config::escalado;
				texturaBoton.height = texturaBoton.width / 2;

				btnReiniciar.color = BLUE;
				btnAlMenu.color = PINK;

				btnReiniciar.textura = texturaBoton;
				btnAlMenu.textura = texturaBoton;

				btnReiniciar.rec.width = texturaBoton.width;
				btnAlMenu.rec.width = texturaBoton.width;

				btnReiniciar.rec.height = texturaBoton.height;
				btnAlMenu.rec.height = texturaBoton.height;

				btnReiniciar.rec.x = GetScreenWidth() / 2 - btnReiniciar.rec.width * 1.5f;
				btnAlMenu.rec.x = GetScreenWidth() / 2 + btnAlMenu.rec.width * 1.5f - btnAlMenu.rec.width;

				btnReiniciar.rec.y = GetScreenHeight() / 2 - btnReiniciar.rec.height / 2;
				btnAlMenu.rec.y = GetScreenHeight() / 2 - btnAlMenu.rec.height / 2;
			}
			void update()
			{
				mouse = GetMousePosition();
				input();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(mouse, btnReiniciar.rec))
					{
						juego::init();
					}
					if (CheckCollisionPointRec(mouse, btnAlMenu.rec))
					{
						menuInmersion--;
						menuActual[menuInmersion] = MENU::PRINCIPAL;
					}
				}
			}

			void draw()
			{
				DrawTextureEx(btnReiniciar.textura, { btnReiniciar.rec.x, btnReiniciar.rec.y }, 0, 1, btnReiniciar.color);
				DrawTextureEx(btnAlMenu.textura, { btnAlMenu.rec.x, btnAlMenu.rec.y }, 0, 1, btnAlMenu.color);

				DrawText("Restart", btnReiniciar.rec.x + btnReiniciar.rec.width / 2 - MeasureText("Restart", txt::tamFuente) / 2, btnReiniciar.rec.y + btnReiniciar.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);
				DrawText("Menu", btnAlMenu.rec.x + btnAlMenu.rec.width / 2 - MeasureText("Menu", txt::tamFuente) / 2, btnAlMenu.rec.y + btnAlMenu.rec.height / 2 - txt::tamFuente / 2, txt::tamFuente, WHITE);

			}
		}
	}
}




