#ifndef MENUES_H
#define MENUES_H

#include "raylib.h"

namespace endless
{
	namespace menues
	{
		enum class MENU { PRINCIPAL, ENDLESS, OPCIONES, CREDITOS, LIMITE };
		extern MENU menuActual[(int)MENU::LIMITE];
		extern int menuInmersion;
		extern Texture2D texturaBoton;

		

		void init();

		namespace principal
		{
			extern Texture2D banderaEn;
			extern Texture2D banderaEs;

			void init();
			void update();
			void input();
			void draw();
		}

		namespace opciones
		{
			void init();
			void update();
			void input();
			void draw();
			void cambiarColorGris();
		}

		namespace creditos
		{
			void init();
			void update();
			void draw();
		}

		namespace pausa
		{
			extern int tiempoPausa[2];
			void init();
			void update();
			void input();
			void draw();
		}

		namespace gameover
		{
			void init();
			void update();
			void input();
			void draw();
		}
	}
}




#endif